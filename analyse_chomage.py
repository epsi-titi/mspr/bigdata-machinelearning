import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_excel('Dataset.xlsx')
df = df.fillna(df.mean())
df = df.dropna(subset=['Nom 1er tour'])


df = df[["taux_chomage_2017", "Nom 1er tour"]]
df_grouped = df.groupby('Nom 1er tour')

# Grouper les données par taux de chômage et compter les occurrences de chaque candidat élu
df_grouped = df.groupby(['taux_chomage_2017', 'Nom 1er tour']).size().unstack()

# Créer un diagramme en barres pour visualiser les résultats
ax = df_grouped.plot(kind='bar', stacked=True, figsize=(10, 6))
ax.set_xlabel('Taux de chômage')
ax.set_ylabel('Nombre de candidats élus')
ax.set_title('Relation entre le taux de chômage et les candidats élus')

plt.legend(title='Candidat', bbox_to_anchor=(1.05, 1), loc='upper left')
plt.tight_layout()
plt.show()

# Créer un nuage de points
plt.figure(figsize=(10, 6))
plt.scatter(df['taux_chomage_2017'], df['Nom 1er tour'])
plt.xlabel('Taux de chômage')
plt.ylabel('Candidat élu')
plt.title('Relation entre le taux de chômage et les candidats élus')
plt.xticks(rotation=45)
plt.grid(True)
plt.show()