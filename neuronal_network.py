import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import LabelEncoder, StandardScaler, MinMaxScaler
from sklearn.impute import SimpleImputer
import matplotlib.pyplot as plt
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense

df = pd.read_excel('Dataset.xlsx')

# Préparation des données
X = df.drop('% Voix/Exp', axis=1)  # toutes les colonnes sauf '% Voix/Exp'
y = df['% Voix/Exp']  # variable cible

# Séparation des colonnes numériques et catégorielles
num_cols = X.select_dtypes(include=[np.number]).columns
cat_cols = X.select_dtypes(exclude=[np.number]).columns

# Remplacer les valeurs manquantes par la moyenne pour les colonnes numériques
imp_num = SimpleImputer(missing_values=np.nan, strategy='mean')
X[num_cols] = imp_num.fit_transform(X[num_cols])

# Remplacer les valeurs manquantes par la valeur la plus fréquente pour les colonnes catégorielles
imp_cat = SimpleImputer(missing_values=np.nan, strategy='most_frequent')
X[cat_cols] = imp_cat.fit_transform(X[cat_cols])

le = LabelEncoder()
for col in cat_cols:
    X[col] = le.fit_transform(X[col])

# Les réseaux de neurones fonctionnent mieux avec les données normalisées
scaler = MinMaxScaler()
X = pd.DataFrame(scaler.fit_transform(X), columns=X.columns)

# Division des données en un ensemble d'entraînement et de test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.6, random_state=42)

# Création du modèle
model = Sequential()
model.add(Dense(50, activation='relu', input_dim=X_train.shape[1]))
model.add(Dense(1))

# Compilation du modèle
model.compile(optimizer='adam', loss='mean_squared_error')

# Entraînement du modèle
history = model.fit(X_train, y_train, epochs=100, batch_size=32, validation_data=(X_test, y_test))

# Prédiction sur l'ensemble de test
y_pred = model.predict(X_test)

# Évaluation du modèle
mse = mean_squared_error(y_test, y_pred)
variance = np.var(y_test)
print(f"Variance des valeurs réelles : {variance}")
print(f"Erreur quadratique moyenne : {mse}")

plt.figure(figsize=(10, 6))
plt.scatter(y_test, y_pred, alpha=0.5)
plt.plot([min(y_test), max(y_test)], [min(y_test), max(y_test)], color='red')  # ligne y=x pour référence
plt.xlabel('Vraies Valeurs')
plt.ylabel('Prédictions')
plt.title('Vraies Valeurs vs Prédictions')
plt.show()

plt.figure(figsize=(10, 6))
y_pred = y_pred.flatten()
residuals = y_test - y_pred
plt.hist(residuals, bins=20, edgecolor='black')
plt.xlabel('Erreurs résiduelles')
plt.ylabel('Compte')
plt.title('Histogramme des erreurs résiduelles')
plt.show()

plt.figure(figsize=(10, 6))
plt.scatter(y_pred, residuals, alpha=0.5)
plt.axhline(y=0, color='r', linestyle='--')
plt.xlabel('Valeurs Prédites')
plt.ylabel('Erreurs Résiduelles')
plt.title('Erreurs Résiduelles vs Valeurs Prédites')
plt.show()

plt.figure(figsize=(10, 6))
plt.plot(history.history['loss'], label='Erreur d\'entraînement')
plt.plot(history.history['val_loss'], label='Erreur de validation')
plt.xlabel('Époque')
plt.ylabel('Erreur')
plt.title('Courbe d\'apprentissage')
plt.legend()
plt.show()