from sklearn.model_selection import train_test_split
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import confusion_matrix
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn import tree
import graphviz

df = pd.read_excel('Dataset.xlsx')
df = df.fillna(df.mean())
df = df.dropna(subset=['Nom 1er tour'])

#récupération des données nécessaires
X = df[["taux_chomage_2017", "Niveau de vie médian 2017", "% 20 à 39 ans 2017", "% 60 à 74 ans 2017"]]  # variable cible
# Préparation des données
y = df['Nom 1er tour']  # variable cible

# Diviser les données en ensembles d'entraînement et de test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Créer le modèle d'arbre de décision pour la classification
model = DecisionTreeClassifier()

# Entraîner le modèle sur l'ensemble d'entraînement
model.fit(X_train, y_train)

# Faire des prédictions sur l'ensemble de test
predictions = model.predict(X_test)

# Évaluer les performances du modèle
accuracy = accuracy_score(y_test, predictions)
print('Accuracy:', accuracy)
# Calculer le taux d'erreur en comparant les prédictions avec les vraies étiquettes de classe
error_rate = 1 - accuracy_score(y_test, predictions)
# Afficher le taux d'erreur
print("Taux d'erreur : {:.2f}".format(error_rate))

# Générer la représentation graphique de l'arbre de décision
dot_data = tree.export_graphviz(model, out_file=None, feature_names=X.columns, class_names=y.unique(), filled=True, rounded=True, special_characters=True)
graph = graphviz.Source(dot_data)

# récupérer le graphique de l'arbre de décision
graph.view()

# Créer une matrice de confusion
cm = confusion_matrix(y_test, predictions)
# Visualiser la matrice de confusion sous forme de heatmap
plt.figure(figsize=(8, 6))
sns.heatmap(cm, annot=True, fmt="d", cmap="Blues", xticklabels=model.classes_, yticklabels=model.classes_)
plt.xlabel("Prédictions")
plt.ylabel("Vraies étiquettes")
plt.title("Matrice de confusion")
plt.show()

# Obtenir l'importance des caractéristiques depuis le modèle d'arbre de décision
feature_importance = model.feature_importances_

# Créer un DataFrame pour visualiser l'importance des caractéristiques
df_importance = pd.DataFrame({"Caractéristique": X.columns, "Importance": feature_importance})

# Trier les caractéristiques par ordre d'importance décroissante
df_importance = df_importance.sort_values(by="Importance", ascending=False)

# Visualiser l'importance des caractéristiques avec un graphique à barres
plt.figure(figsize=(8, 6))
sns.barplot(x="Importance", y="Caractéristique", data=df_importance, palette="viridis")
plt.xlabel("Importance")
plt.ylabel("Caractéristique")
plt.title("Importance des caractéristiques dans l'arbre de décision")
plt.show()