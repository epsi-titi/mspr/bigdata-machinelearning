import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, LabelEncoder
import matplotlib.pyplot as plt
from collections import Counter
import tensorflow as tf

# Charger les données à partir du DataFrame
df = pd.read_excel('/content/drive/MyDrive/EPSI/Master1/MSPR DATA/Dataset.xlsx')
df = df.fillna(df.mean())
df = df.dropna(subset=['Nom 1er tour'])

# Sélectionner les caractéristiques pertinentes pour l'entraînement
X = df[["taux_chomage_2017", "Niveau de vie médian 2017","% 20 à 39 ans 2017", "% 40 à 59 ans 2017", "% 60 à 74 ans 2017", "% 75 ans et plus 2017"]]

# Préparer les étiquettes des candidats (assurez-vous que "Nom 1er tour" est encodé sous forme d'entiers)
label_encoder = LabelEncoder()
y = label_encoder.fit_transform(df['Nom 1er tour'])

# Préparer les taux de voix comme cibles pour la régression
voix = df['% Voix/Exp']
# Diviser les données en ensembles d'entraînement et de test
X_train, X_test, y_train, y_test, voix_train, voix_test = train_test_split(X, y, voix, test_size=0.2, random_state=42)
# Mettre à l'échelle les caractéristiques
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

# Entraîner le modèle de classification multiclasse
model_classification = tf.keras.models.Sequential([
    tf.keras.layers.Dense(64, activation='relu', input_shape=(X_train_scaled.shape[1],)),
    tf.keras.layers.Dense(32, activation='relu'),
    tf.keras.layers.Dense(len(label_encoder.classes_), activation='softmax')
])

model_classification.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])

history_classification = model_classification.fit(X_train_scaled, y_train, epochs=50, batch_size=16, validation_split=0.1)

# Entraîner le modèle de régression pour prédire les taux de voix
model_regression = tf.keras.models.Sequential([
    tf.keras.layers.Dense(64, activation='relu', input_shape=(X_train_scaled.shape[1],)),
    tf.keras.layers.Dense(32, activation='relu'),
    tf.keras.layers.Dense(1, activation='linear')
])


model_regression.compile(optimizer='adam', loss='mean_squared_error', metrics=['mae'])

history_regression = model_regression.fit(X_train_scaled, voix_train, epochs=20, batch_size=8, validation_split=0.1)

# Évaluation du modèle de classification
test_loss_classification, test_accuracy_classification = model_classification.evaluate(X_test_scaled, y_test)
print(f"Accuracy on test set (classification): {test_accuracy_classification}")

# Évaluation du modèle de régression
test_loss_regression, test_mae_regression = model_regression.evaluate(X_test_scaled, voix_test)
print(f"Mean Absolute Error on test set (regression): {test_mae_regression}")


# Affichage des courbes Loss et accuracy pendant l'entraînement du modèle de classification
plt.figure(figsize=(12, 5))
plt.subplot(1, 2, 1)
plt.plot(history_classification.history['loss'], label='Training Loss')
plt.plot(history_classification.history['val_loss'], label='Validation Loss')
plt.xlabel('Epoch')
plt.ylabel('Loss')
plt.title('Training and Validation Loss (Classification)')
plt.legend()

plt.subplot(1, 2, 2)
plt.plot(history_classification.history['accuracy'], label='Training Accuracy')
plt.plot(history_classification.history['val_accuracy'], label='Validation Accuracy')
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.title('Training and Validation Accuracy (Classification)')
plt.legend()

plt.tight_layout()
plt.show()

# Prédiction des classes (candidats) sur l'ensemble de test
predictions_classification = model_classification.predict(X_test_scaled)

# Récupérer les classes prédites (indices des candidats gagnants)
predicted_classes = np.argmax(predictions_classification, axis=1)

# Décodez les classes prédites en noms de candidats
predicted_candidates = label_encoder.inverse_transform(predicted_classes)

# Compter le nombre de voix pour chaque candidat
votes_count = Counter(predicted_candidates)

# Nombre total de votes dans l'ensemble de test
total_votes = len(predicted_candidates)

# Calculer le pourcentage de voix pour chaque candidat
votes_percentage = {candidate: (count / total_votes) * 100 for candidate, count in votes_count.items()}

# Créer le graphique d'attribution des voix par candidat
plt.figure(figsize=(8, 6))
plt.bar(votes_percentage.keys(), votes_percentage.values())
plt.xlabel('Candidat')
plt.ylabel('Pourcentage de Voix')
plt.title('Attribution des Voix par Candidat')
plt.xticks(rotation=45)
plt.show()

