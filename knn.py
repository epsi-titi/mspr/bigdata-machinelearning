import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsRegressor  # <-- Ajout de KNeighborsRegressor
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import LabelEncoder
from sklearn.impute import SimpleImputer
import matplotlib.pyplot as plt
from sklearn.metrics import median_absolute_error

df = pd.read_excel('Dataset.xlsx')

# Préparation des données
X = df.drop('% Voix/Exp', axis=1)  # toutes les colonnes sauf '% Voix/Exp'
y = df['% Voix/Exp']  # variable cible

# Séparation des colonnes numériques et catégorielles
num_cols = X.select_dtypes(include=[np.number]).columns
cat_cols = X.select_dtypes(exclude=[np.number]).columns

# Remplacer les valeurs manquantes par la moyenne pour les colonnes numériques
imp_num = SimpleImputer(missing_values=np.nan, strategy='mean')
X[num_cols] = imp_num.fit_transform(X[num_cols])

# Remplacer les valeurs manquantes par la valeur la plus fréquente pour les colonnes catégorielles
imp_cat = SimpleImputer(missing_values=np.nan, strategy='most_frequent')
X[cat_cols] = imp_cat.fit_transform(X[cat_cols])

le = LabelEncoder()
for col in cat_cols:
    X[col] = le.fit_transform(X[col])

# Division des données en un ensemble d'entraînement et de test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Création et entraînement du modèle
model = KNeighborsRegressor(n_neighbors=5)  # <-- Création du modèle KNN
model.fit(X_train, y_train)

# Prédiction sur l'ensemble de test
y_pred = model.predict(X_test)

# Évaluation du modèle
mse = mean_squared_error(y_test, y_pred)
variance = np.var(y_test)
print(f"Variance des valeurs réelles : {variance}")
print(f"Erreur quadratique moyenne : {mse}")
medae = median_absolute_error(y_test, y_pred)
print(f"Erreur médiane absolue : {medae}")
print(f"Score du modèle : {int(model.score(X_test, y_test)*100)} %")

plt.figure(figsize=(10, 6))
plt.scatter(y_test, y_pred, alpha=0.5)
plt.plot([min(y_test), max(y_test)], [min(y_test), max(y_test)], color='red')  # ligne y=x pour référence
plt.xlabel('Vraies Valeurs')
plt.ylabel('Prédictions')
plt.title('Vraies Valeurs vs Prédictions')
plt.show()

plt.figure(figsize=(10, 6))
residuals = y_test - y_pred
plt.hist(residuals, bins=20, edgecolor='black')
plt.xlabel('Erreurs résiduelles')
plt.ylabel('Compte')
plt.title('Histogramme des erreurs résiduelles')
plt.show()

plt.figure(figsize=(10, 6))
plt.scatter(y_pred, residuals, alpha=0.5)
plt.axhline(y=0, color='r', linestyle='--')
plt.xlabel('Valeurs Prédites')
plt.ylabel('Erreurs Résiduelles')
plt.title('Erreurs Résiduelles vs Valeurs Prédites')
plt.show()

from sklearn.model_selection import learning_curve

train_sizes, train_scores, test_scores = learning_curve(model, X, y, cv=5)
train_scores_mean = np.mean(train_scores, axis=1)
test_scores_mean = np.mean(test_scores, axis=1)

plt.figure(figsize=(10, 6))
plt.plot(train_sizes, train_scores_mean, label='Score d\'entraînement')
plt.plot(train_sizes, test_scores_mean, label='Score de validation')
plt.xlabel('Taille de l\'échantillon d\'entraînement')
plt.ylabel('Score')
plt.title('Courbe d\'apprentissage')
plt.legend()
plt.show()