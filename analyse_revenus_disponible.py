import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_excel('Dataset.xlsx')
df = df.fillna(df.mean())
df = df.dropna(subset=['Nom 1er tour'])

df = df[["Niveau de vie médian 2017", "Nom 1er tour"]]
df_grouped = df.groupby('Nom 1er tour')
# Définir les intervalles de catégories pour le revenu disponible
intervals = range(14000, 30000, 2000)  # fourchettes de 0 à 50000 par pas de 5000

# Créer une nouvelle colonne "cat_revenus" avec les catégories basées sur le revenu disponible
df['cat_revenus'] = pd.cut(df['Niveau de vie médian 2017'], bins=intervals)

# Grouper les données par taux de chômage et compter les occurrences de chaque candidat élu
df_grouped = df.groupby(['cat_revenus', 'Nom 1er tour']).size().unstack()


# Créer un diagramme en barres pour visualiser les résultats
ax = df_grouped.plot(kind='bar', stacked=True, figsize=(10, 6))
ax.set_xlabel('Revenus disponibles')
ax.set_ylabel('Nombre de candidats élus')
ax.set_title('Relation entre le revenus disponibles et les candidats élus')

plt.legend(title='Candidat', bbox_to_anchor=(1.05, 1), loc='upper left')
plt.tight_layout()
plt.show()

# Créer un nuage de points
plt.figure(figsize=(10, 6))
plt.scatter(df['Niveau de vie médian 2017'], df['Nom 1er tour'])
plt.xlabel('Revenus disponibles')
plt.ylabel('Candidat élu')
plt.title('Relation entre le revenus disponibles et les candidats élus')
plt.xticks(rotation=45)
plt.grid(True)
plt.show()